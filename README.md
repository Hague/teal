# Teal

[Slideshow S9][slideshow-s9] converts markdown to HTML slides. Teal is a
super-simple HTML slides template for use with Slideshow S9.

The template

* produces a single HTML file that can be easily distributed,
* does not use any JavaScript.

The use case is short presentations during meetings where slides will be
shared with non-technical colleagues.

## Sample

Sample slides are available [here][sample].

## Usage

Install [Slideshow S9][slideshow-s9]:

    $ gem install slideshow

Clone this repository into `~/.slideshow/templates/teal`

    $ mkdir -p ~/.slideshow/templates
    $ cd ~/.slideshow/templates
    $ git clone https://gitlab.com/Hague/teal

Create some slides. Use markdown with a title and author.

    title: My title
    author: Myself

    # Here is the first slide title

    Here are the slide contents

    * One
    * Two
    * Three

    # Here is the second slide title

    And so on

Run `slideshow` to compile into an HTML file

    $ slideshow build -t teal myslides.md

[sample]: https://hague.gitlab.io/teal/index.html
[slideshow-s9]: https://slideshow-s9.github.io/
