title: Sample Teal Slides
author: Matthew Hague

# This is the first slide

These slides demonstrate the [teal][teal] Slideshow S9 template.

* You can have bullet point.
* And bullet points.
* As many as you like.

To get to the next slide, click the arrow on the bottom right.

You can also use page down, which may or may not work cleanly!

# The is the second slide

That's it for this slide package.

It doesn't do much, but that's the point.

This is a single HTML file that

* can be distributed,
* doesn't need any JavaScript to work.

# This is the third slide

All done!

[teal]: https://gitlab.com/Hague/teal
